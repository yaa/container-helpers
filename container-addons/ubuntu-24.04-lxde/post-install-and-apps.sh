cat /etc/apt/sources.list.d/ubuntu.sources || exit 1

apt update || exit 1
apt upgrade

apt clean

apt install bash-completion
dpkg-reconfigure locales

dpkg-reconfigure tzdata

apt install mc
apt install htop

apt install mesa-utils
apt --no-install-recommends install lxde

apt install dirmngr

apt install x11-utils
apt install xinput
apt install x11-apps

apt install x11-xserver-utils x11-xkb-utils xcape

apt install python3-gpg

# apt install alsa-utils
# apt install pipewire-alsa

# apt install libnvidia-gl-470

# apt install neverball

# apt-get install xdg-utils

# for teamspeak
# apt install libnss3
# apt install libevent-2.1-7
# apt install libpci3
# apt install libxslt1.1
# apt install libatomic1
