cat /etc/apt/sources.list

apt-get update || exit 1
apt-get dist-upgrade

apt-get install bash-completion
apt-get install locales
dpkg-reconfigure locales

dpkg-reconfigure tzdata

apt-get install mc
apt-get install htop

apt-get install dirmngr curl apt-transport-https
