cat /etc/apt/sources.list.d/ubuntu.sources || exit 1

apt update || exit 1
apt upgrade

apt clean

apt install bash-completion
dpkg-reconfigure locales

dpkg-reconfigure tzdata

apt install mc
apt install htop

apt install mesa-utils
apt --no-install-recommends install lxde

apt install dirmngr

apt install x11-utils
apt install xinput
apt install x11-apps

apt install x11-xserver-utils x11-xkb-utils xcape

apt install python3-gpg


apt clean


dpkg --add-architecture i386 || exit 1
apt update || exit 1

apt install libasound2t64
apt install libasound2t64:i386

apt install alsa-utils
apt install pipewire-alsa

apt install alsa-utils:i386
apt install pipewire-alsa:i386

apt install xterm


# useful for 32-bit OpenGL apps

# apt install zlib1g:i386
# apt install libgl1:i386

# apt install libnvidia-gl-470
# apt install libnvidia-gl-470:i386

# apt install libglu1-mesa
# apt install libglu1-mesa:i386
